﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAnalysis
{
    public class DayTemperatureData
    {
        public DayTemperatureData(int day)
        {
            this.Day = day;
        }

        public int Day { get; private set; }

        public float MaximumTemperature { get; set; }

        public float MinimumTemperature { get; set; }

        public float Spread
        {
            get
            {
                return MaximumTemperature - MinimumTemperature;
            }
        }
    }
}
