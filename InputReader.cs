﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAnalysis
{
    class InputReader
    {
        public List<DayTemperatureData> GetTemperatureData(string filename)
        {
            var data = new List<DayTemperatureData>();

            var lines = File.ReadAllLines(filename);
            foreach (var line in lines)
            {
                if (isDataLine(line))
                {
                    var words = GetWords(line);
                    var dataItem = new DayTemperatureData(int.Parse(words[0]));
                    if (words.Count > 1)
                    {
                        dataItem.MaximumTemperature = GetTemperature(words[1]);
                        if (words.Count > 2)
                        {
                            dataItem.MinimumTemperature = GetTemperature(words[2]);
                        }
                    }
                    data.Add(dataItem);
                }
            }

            return data;
        }

        private float GetTemperature(string value)
        {
            float temp;
            if (!String.IsNullOrWhiteSpace(value) && float.TryParse(value.Replace("*", String.Empty), out temp))
            {
                return temp;
            }
            return 0f;
        }

        private bool isDataLine(string line)
        {
            if (line == null || line.Length == 0)
                return false;

            var words = GetWords(line);
            int day;
            return int.TryParse(words[0], out day);
        }

        private List<string> GetWords(string line)
        {
            return line.Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
