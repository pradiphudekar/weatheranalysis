﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherAnalysis
{
    class Program
    {
        static void Main(string[] args)
        {
            var filename = "weather.dat";
            if (args.Length > 0)
            {
                filename = args[0];
            }
            var reader = new InputReader();
            var data = reader.GetTemperatureData(filename);

            var dayWithMinimumSpread = data.OrderBy(d => d.Spread).First(); ;
            var coldestDay = data.OrderBy(d => d.MinimumTemperature).First();
            var hottestDay = data.OrderBy(d => d.MaximumTemperature).Reverse().First();

            Console.WriteLine("Day with smallest spread : {0} , Spread : {1}", dayWithMinimumSpread.Day, dayWithMinimumSpread.Spread);

            Console.WriteLine("Coldest day : {0} , Temperature : {1}", coldestDay.Day, coldestDay.MinimumTemperature);

            Console.WriteLine("Hottest day : {0} , Temperature : {1}", hottestDay.Day, hottestDay.MaximumTemperature);

            Console.ReadKey();
        }
    }
}
